﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Cliente:Persona
    {
        public int CodigoPiletaInstalada { get; set; }
        public DateTime FechaInstalacion { get; set; }

        //Corrección: si el método no cambia el comportamiento del método base, no se debe sobreescribir.
        public override string DevolverDatosUsuario()
        {
            return base.DevolverDatosUsuario();
        }

        public override string MostrarMensaje()
        {
            //Corrección: esta condición no funcionaría si por ejemplo la fecha de instalación es a las 18hs, ya que el Today.Date devuelve la fecha con horas 00.
            if (DateTime.Today.Date == FechaInstalacion.Date.AddYears(1))
            {
                return "¡Felicitaciones por su primer aniversario de instalación!";
            }
            return null;
        }
    }
}
