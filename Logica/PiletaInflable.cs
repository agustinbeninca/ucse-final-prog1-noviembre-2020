﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class PiletaInflable:Pileta
    {
        public decimal Diametro { get; set; }
        public decimal Profundidad { get; set; }
        public bool ConCubrePiletas { get; set; } //true=si false=no

        public override int CalcularPorcentajeDescuento()
        {
            return 0;
        }
    }
}
