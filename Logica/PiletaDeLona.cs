﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class PiletaDeLona:Pileta
    {
        public decimal Alto { get; set; }
        public decimal Ancho { get; set; }
        public decimal Profundidad { get; set; }
        public bool ConFiltro { get; set; } //true=si false=no
        public bool ConCubrePiletas { get; set; } //true=si false=no

        public override int CalcularPorcentajeDescuento()
        {
            return 8;
        }
    }
}
