﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Pileta
    {
        public int Codigo { get; set; }
        public decimal CantidadLitros { get; set; }
        public decimal Precio { get; set; }
        public Colores Color { get; set; }

        public enum Colores {Azul,Celeste,Gris}

        //Corrección: Podría ser un método virtual que por defecto devuelva 0 y esto permitiría que las subclases que no lo necesiten no deben sobreescribir el método.
        public abstract int CalcularPorcentajeDescuento();
            
    }
}
