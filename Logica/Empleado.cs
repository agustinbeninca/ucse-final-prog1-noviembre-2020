﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Empleado:Persona
    {
        public DateTime FechaNacimiento { get; set; }
        public Turnos Turno { get; set; }
        public Areas Area { get; set; }

        public enum Turnos {Mañana, Tarde}
        public enum Areas {Gerencia, Ventas, Instalacion}

        public override string DevolverDatosUsuario()
        {
            return base.DevolverDatosUsuario() + $" - Turno: {Turno} - Área: {Area}";
        }

        public override string MostrarMensaje()
        {
            if (DateTime.Today.Month == FechaNacimiento.Month && DateTime.Today.Day == FechaNacimiento.Day)
            {
                return "¡Feliz cumpleaños compañero!";
            }
            return null;
        }
    }
}
