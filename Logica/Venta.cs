﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Venta
    {
        public string DniComprador { get; set; }
        public int CodigoPileta { get; set; }
        public decimal Monto { get; set; }
        public DateTime FechaVenta { get; set; }


    }
}
