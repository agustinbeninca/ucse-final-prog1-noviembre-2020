﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class PiletaDeMaterial:Pileta
    {
        public decimal Largo { get; set; }
        public decimal Ancho { get; set; }
        public double Profundidad { get; set; }
        public bool TieneTrampolin { get; set; } //true=tiene, false=no tiene
        public int CantidadEscaleras { get; set; }

        public override int CalcularPorcentajeDescuento()
        {
            if (Profundidad>1.5 && TieneTrampolin==true)
            {
                return 10;
            }
            return 5;
        }
    }
}
