﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Principal
    {
        public List<Persona> Personas { get; set; }
        public List<Venta> Ventas { get; set; }
        public List<Pileta> Piletas { get; set; }

        public bool  RegistrarCompraPileta(string dniComprador, int codigoPileta)
        {
            Venta nuevaVenta = new Venta();
            nuevaVenta.DniComprador = dniComprador;
            nuevaVenta.CodigoPileta = codigoPileta;
            nuevaVenta.FechaVenta = DateTime.Today;

            foreach (var pileta in Piletas)
            {
                if (pileta.Codigo==codigoPileta)
                {
                    foreach (var item in Personas)
                    {
                        if (dniComprador == item.Dni && item is Empleado)
                        {
                            nuevaVenta.Monto = pileta.Precio - (pileta.Precio * pileta.CalcularPorcentajeDescuento()/100);
                            Ventas.Add(nuevaVenta);

                            //Corrección: El método no devuelve el resultado como se solicita. Un método solo puede tener UN RETURN. Además, no utilizas el valor que devuelve el método Msg
                            Msg("El registro se realizó correctamente!");
                            return true;
                        }
                        else if (dniComprador == item.Dni && item is Cliente)
                        {
                            Cliente cliente = item as Cliente;
                            cliente.FechaInstalacion = DateTime.Today;
                            cliente.CodigoPiletaInstalada = codigoPileta;
                            nuevaVenta.Monto = pileta.Precio;
                            Ventas.Add(nuevaVenta);
                            //Corrección: El método no devuelve el resultado como se solicita. Un método solo puede tener UN RETURN. Además, no utilizas el valor que devuelve el método Msg
                            Msg("Se registró la nueva pileta del cliente.");
                            return true;
                        }
                        else
                        {
                            Cliente nuevoCliente = new Cliente();
                            nuevoCliente.Dni = dniComprador;
                            nuevoCliente.CodigoPiletaInstalada = codigoPileta;
                            nuevoCliente.FechaInstalacion = DateTime.Today;
                            Personas.Add(nuevoCliente);
                            nuevaVenta.Monto = pileta.Precio;
                            Ventas.Add(nuevaVenta);
                            //Corrección: El método no devuelve el resultado como se solicita. Un método solo puede tener UN RETURN. Además, no utilizas el valor que devuelve el método Msg
                            Msg("Se dio de alta la venta y el cliente, recuerde que debe completar sus datos");
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        public string Msg(string mensaje)
        {
            return mensaje;
        }
    }
}
