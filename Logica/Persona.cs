﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public abstract class Persona
    {
        public string Dni { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public Localidad Localidad { get; set; }


        public virtual string DevolverDatosUsuario()
        {
            return $"{Apellido}, {Nombre} - Teléfono: {Telefono} - {Localidad.Provincia}, {Localidad.Nombre}, {Direccion}";
        }

        public abstract string MostrarMensaje();
    }
}
